package com.example.memory_kt.model

class Distribution: Iterable<CardSymbol> {
    private val  cards = CardSymbol
        .values()
        .plus(CardSymbol.values())
        .toList()
        .shuffled()


    override fun iterator() = cards.iterator()
}