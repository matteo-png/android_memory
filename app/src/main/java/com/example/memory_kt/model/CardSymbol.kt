package com.example.memory_kt.model

enum class CardSymbol {
    CHAT,
    CHIEN,
    SOURIS,
    LOUP,
    HIBOU,
    CERF
}