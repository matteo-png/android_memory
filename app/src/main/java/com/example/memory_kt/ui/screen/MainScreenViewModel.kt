package com.example.memory_kt.ui.screen

import androidx.lifecycle.ViewModel
import com.example.memory_kt.model.Distribution
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow


class MainScreenViewModel : ViewModel() {
    private val _distribution: MutableStateFlow<Distribution> = MutableStateFlow(Distribution())
    val distribution: StateFlow<Distribution> = _distribution

    private val _firstSelection: MutableStateFlow<Int?> = MutableStateFlow(null)
    val firstSelection : StateFlow<Int?> = _firstSelection

    private val _secondSelection: MutableStateFlow<Int?> = MutableStateFlow(null)
    val secondSelection : StateFlow<Int?> = _secondSelection

    fun play(position: Int){
        _firstSelection.value = position
    }
}