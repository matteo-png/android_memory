package com.example.memory_kt.ui.screen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.memory_kt.model.CardSymbol
import com.example.memory_kt.model.Distribution
import com.example.memory_kt.ui.composable.Card



@Composable
fun MainScreen(viewModel: MainScreenViewModel) {
    val distribution by viewModel.distribution.collectAsState()
    val firstSelection by viewModel.firstSelection.collectAsState()
    val secondSelection by viewModel.secondSelection.collectAsState()

    MainScreen(
        distribution = distribution,
        firstSelection = firstSelection,
        secondSelection = secondSelection,
        onCardPlayed = viewModel::play
    )
}


@Composable
@OptIn(ExperimentalLayoutApi::class)
private fun MainScreen(
    distribution: Distribution,
    firstSelection: Int?,
    secondSelection: Int?,
    onCardPlayed: (Int) -> Unit,
) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center,
    ) {
        Row(

        ) {
            for ((i, card) in distribution.withIndex()) {
                Card(
                    modifier = Modifier.clickable { onCardPlayed(i) },
                    symbol = card ?: CardSymbol.CHAT,
                    returned = i == firstSelection || i == secondSelection,
                )
            }
        }
    }
}

@Preview
@Composable
private fun MainScreenPreview() {
    MainScreen(
        distribution = Distribution(),
        firstSelection = null,
        secondSelection = null,
        onCardPlayed = {},
    )
}