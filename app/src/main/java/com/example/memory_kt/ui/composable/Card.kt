package com.example.memory_kt.ui.composable

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.memory_kt.R
import com.example.memory_kt.model.CardSymbol


private const val CARD_SIZE = 128;

@Composable
fun Card(modifier: Modifier = Modifier, symbol: CardSymbol, returned: Boolean) {
    Box(
        modifier = modifier
            .width(CARD_SIZE.dp)
            .height(CARD_SIZE.dp)
            .background(Color.LightGray),
        contentAlignment = Alignment.Center
    ) {
        if (returned) {
            Image(
                painter = painterResource(id = symbol.getDrawable()),
                contentDescription = "",
            )
        }
    }
}

private fun CardSymbol.getDrawable(): Int{
    return when(this){
        CardSymbol.CHAT -> R.drawable.chat
        CardSymbol.CHIEN -> R.drawable.chien
        CardSymbol.SOURIS -> R.drawable.souris
        CardSymbol.LOUP -> R.drawable.loup
        CardSymbol.HIBOU -> R.drawable.hibou
        CardSymbol.CERF -> R.drawable.cerf
    }
}




@Preview(showBackground = true)
@Composable
private fun CardPreview() {
    Card(symbol = CardSymbol.CERF, returned = true)
}
